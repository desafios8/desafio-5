
public class Pessoa {
	private String nome;
	private String codigo;
	private int codigo_cargo;
		
	public Pessoa() {
		
	}

	public Pessoa(String nome, String codigo, int codigo_cargo) 
	{
		this.nome = nome;
		this.codigo = codigo;
		this.codigo_cargo = codigo_cargo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public int getCodigo_cargo() {
		return codigo_cargo;
	}
	public void setCodigo_cargo(int codigo_cargo) {
		this.codigo_cargo = codigo_cargo;
	}
	
	

}
