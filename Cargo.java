
public class Cargo {
	private float salario;
	
	public Cargo() {
		
	}

	public Cargo(float salario) {
		this.salario = salario;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}
	
	

}
