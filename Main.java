import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner entrada = new Scanner (System.in);
		Menu menu = new Menu();
		Cargo cargo = new Cargo();
		Pessoa pessoa = new Pessoa();
		String nome, codigo;
		int tipo, op;
		float salario;
		do 
		{
			System.out.println();
			System.out.println("Menu");
			System.out.println("");
			System.out.println("1-Cadastrar cargo");
			System.out.println("2-Cadastrar funcionario");
			System.out.println("3-Mostrar relatorio");
			System.out.println("4-Mostrar total de salario por cargo");
			System.out.println("5-Sair");
			op = entrada.nextInt();
			if(op<1||op>5)
				System.out.println("Op��o invalida");
			if(op==1) 
			{
				System.out.println("Digite o valor do salario do cargo '"+menu.proximo_Cargo()+"'");
				salario = entrada.nextFloat();
				cargo.setSalario(salario);
				menu.cadastrar_Cargo(cargo.getSalario());
			}
			if(op==2) 
			{
				System.out.println("Digite o nome do funcionario");
				nome = entrada.next();
				pessoa.setNome(nome);
				System.out.println("Digite o codigo do funcionario");
				codigo = entrada.next();
				pessoa.setCodigo(codigo);
				System.out.println("Digite o cargo do funcionario");
				tipo = entrada.nextInt();
				pessoa.setCodigo_cargo(tipo);
				menu.adicionar(pessoa.getNome(), pessoa.getCodigo(), pessoa.getCodigo_cargo());
			}
			if(op==3) 
			{
				menu.relatorio();
			}
			if(op==4) 
			{
				System.out.println("Digite o cargo a ser consultado");
				tipo = entrada.nextInt();
				System.out.println(menu.salario_Cargo(tipo));
			}
		}while(op!=5);

	}

}
