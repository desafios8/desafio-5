import java.util.ArrayList;

public class Menu {
	private ArrayList<Pessoa> funcionario = new ArrayList<Pessoa>();
	private ArrayList<Cargo> cargo = new ArrayList<Cargo>();
	
	public boolean existe_Cargo(int valor) 
	{
		if(valor>cargo.size()||valor<0) 
		{
			System.out.println("Esse cargo n�o existe");	
			return false;
	    }
		else return true;
	}
	
	public boolean existe_Funcionario(String nome) 
	{
		for (int i = 0; i < funcionario.size(); i++) 
		{
	        if (nome==funcionario.get(i).getNome()) {

	            System.out.println("O funcionario ja est� cadastrado");
	            return true;
	        }
		}
		return false;
	}
	
	public void cadastrar_Cargo(float salario) 
	{
		Cargo tipo = new Cargo(salario);
		cargo.add(tipo);
	}
	
	public void adicionar(String nome, String codigo, int cargo) 
	{
		
		if(existe_Cargo(cargo)==true&&existe_Funcionario(nome)==false) 
		{
		Pessoa pessoa = new Pessoa(nome,codigo,cargo);
		funcionario.add(pessoa);
		}
	}
	
	public void relatorio() 
	{
		for (int i = 0; i < funcionario.size(); i++) 
		{
	        System.out.println("\n"+"Nome: "+ funcionario.get(i).getNome());
	        System.out.println("\n"+"Codigo: "+ funcionario.get(i).getCodigo());
	        System.out.println("\n"+"Cargo: "+ funcionario.get(i).getCodigo_cargo());
	        System.out.println("-------------------------------------------");
		}
	}
	
	public float salario_Cargo(int tipo) 
	{
		float salario = 0;
		if(existe_Cargo(tipo)==true) 
		{
			for (int i = 0; i < funcionario.size(); i++) 
			{
		        if(tipo==funcionario.get(i).getCodigo_cargo()) 
		        {
		        	salario = salario + cargo.get(tipo).getSalario();
		        }
			}
		}
		
		return salario;
	}
	
	public int proximo_Cargo() 
	{
		int i = 0;
		i = cargo.size();
		return i;
	}

}
